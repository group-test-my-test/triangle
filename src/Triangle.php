<?php

namespace Davihedg\Triangle;

class Triangle
{
    /**
     * Find all sides and angles of triangle
     *
     * @param $arguments
     * @return array|string
     */
    public function character($arguments)
     {
         extract($arguments, EXTR_PREFIX_SAME, "wddx");

        if (isset($a, $b, $c) && (is_null($angle_A) || is_null($angle_B) || is_null($angle_C))){
            $answer = new Character();
            $triangle = $answer->threeSide($a, $b, $c);
        }elseif (isset($a, $b, $angle_C) && (is_null($c) || is_null($angle_A) || is_null($angle_B))) {
            $answer = new Character;
            $triangle = $answer->sideAngleSide($a, $b, $angle_C);
        }elseif (isset($b, $c, $angle_A) && (is_null($a) || is_null($angle_B) || is_null($angle_C))) {
            $answer = new Character;
            $triangle = $answer->sideAngleSide($b, $c, $angle_A);
        }elseif (isset($a, $c, $angle_B) && (is_null($b) || is_null($angle_A) || is_null($angle_C))) {
            $answer = new Character;
            $triangle = $answer->sideAngleSide($a, $c, $angle_B);
        }elseif (isset($a, $b, $angle_A) && (is_null($c) && is_null($angle_C))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($a, $b, $angle_A);
        }elseif (isset($a, $b, $angle_B) && (is_null($c) && is_null($angle_C))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($a, $b, $angle_B);
        }elseif (isset($a, $c, $angle_A) && (is_null($b) && is_null($angle_B))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($a, $c, $angle_A);
        }elseif (isset($a, $c, $angle_C) && (is_null($b) && is_null($angle_B))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($a, $c, $angle_C);
        }elseif (isset($b, $c, $angle_B) && (is_null($a) && is_null($angle_A))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($b, $c, $angle_B);
        }elseif (isset($b, $c, $angle_C) && (is_null($a) && is_null($angle_A))) {
            $answer = new Character;
            $triangle = $answer->sideSideAngle($b, $c, $angle_C);
        }elseif (isset($c, $angle_A, $angle_B) && (is_null($a) || is_null($b) || is_null($angle_C))) {
            $answer = new Character;
            $triangle = $answer->angleSideAngle($c, $angle_A, $angle_B);
        }elseif (isset($b, $angle_A, $angle_C) && (is_null($a) || is_null($c) || is_null($angle_B))) {
            $answer = new Character;
            $triangle = $answer->angleSideAngle($b, $angle_A, $angle_C);
        }elseif (isset($a, $angle_B, $angle_C) && (is_null($b) || is_null($c) || is_null($angle_A))) {
            $answer = new Character;
            $triangle = $answer->angleSideAngle($a, $angle_B, $angle_C);
        }else {
            $triangle = 'Ошибка! Недостаточно данных для построения треугольника';
        }

        return $triangle;
     }

    /**
     *Calculate the perimeter of triangle
     *
     * @param $arguments
     * @return float|int|string
     */
    public function perimeter($arguments)
     {
         extract($arguments, EXTR_PREFIX_SAME, "wddx");

         if (is_null($a) || is_null($b) || is_null($c)) {
             $perimeter = 'Ошибка! Недостаточно данных';
         }else {
             $perimeter = $a + $b + $c;
         }

         return $perimeter;
     }

    /**
     * Calculate the square of triangle
     *
     * @param $arguments
     * @return float|int|string
     */
    public function square($arguments)
     {
         extract($arguments, EXTR_PREFIX_SAME, "wddx");
         $angle_C = (pi()*$angle_C)/180;

         if (is_null($a) || is_null($b) || is_null($angle_C)) {
             $answer = 'Ошибка! Недостаточно данных';
         }else {
             $answer = ($a * $b * sin($angle_C))/2;
         }

         return $answer;
     }
}