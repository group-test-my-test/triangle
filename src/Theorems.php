<?php

namespace Davihedg\Triangle;

class Theorems
{
    /**
     * Cosine theorem for angle
     *
     * @param $a
     * @param $b
     * @param $c
     * @return float
     */
    public function cosAngle($a, $b, $c)
    {
        $angle = acos(($b*$b+$c*$c-$a*$a)/(2*$b*$c));
        $angle = ceil((180*$angle)/pi());

        return $angle;
    }

    /**
     * Cosine theorem for side
     *
     * @param $a
     * @param $b
     * @param $angle_C
     * @return float
     */
    public function cosSide($a, $b, $angle_C)
    {
        $angle_C = (pi()*$angle_C)/180;
        $side = ceil(sqrt($a*$a+$b*$b-2*$a*$b*cos($angle_C)));

        return $side;
    }

    /**
     * Find third angle using property of the sum of the angles of triangle
     *
     * @param $angle_one
     * @param $angle_two
     * @return int
     */
    public function thirdAngle($angle_one, $angle_two)
    {
        $angle_three = 180 - ($angle_one+$angle_two);

        return $angle_three;
    }

    /**
     * Sine theorem for angle
     *
     * @param $a
     * @param $b
     * @param $angle_A
     * @return float
     */
    public function sinAngle($a, $b, $angle_A)
    {
        $angle_A = (pi()*$angle_A)/180;
        $angle = round(($b*sin($angle_A))/$a, 1);
        return $angle;
    }

    /**
     * Sine theorem for side
     *
     * @param $b
     * @param $angle_A
     * @param $angle_B
     * @return float
     */
    public function sinSide($b, $angle_A, $angle_B)
    {
        $angle_A = (pi()*$angle_A)/180;
        $angle_B = (pi()*$angle_B)/180;
        $side = round(($b*sin($angle_A))/sin($angle_B),1);

        return $side;
    }
}