<?php

namespace Davihedg\Triangle;

class Character extends Theorems
{
    /**
     * Find all angles using the sides of triangle
     *
     * @param $a
     * @param $b
     * @param $c
     * @return array|string
     */
    public function threeSide($a, $b, $c)
    {
        if ((($a+$b)<$c)or(($a+$c)<$b)or(($c+$b)<$a)){
            $answer = 'Ошибка! Построение треугольника невозможно, проверьте данные';
        }else {
            $angle_A = $this->cosAngle($a, $b, $c);
            $angle_B = $this->cosAngle($b, $a, $c);
            $angle_C = $this->thirdAngle($angle_A, $angle_B);

            $answer = [
                "a" => $a,
                "b" => $b,
                "c" => $c,
                "angle_A" => $angle_A,
                "angle_B" => $angle_B,
                "angle_C" => $angle_C,
            ];
        }

        return $answer;
    }

    /**
     * Find side and angles of triangle using two sides and angle between them
     *
     * @param $a
     * @param $b
     * @param $angle_C
     * @return array
     */
    public function sideAngleSide($a, $b, $angle_C)
    {
        $c = $this->cosSide($a, $b, $angle_C);
        $angle_A = $this->cosAngle($a, $b, $c);
        $angle_B = $this->thirdAngle($angle_A, $angle_C);

        $answer = [
            "a" => $a,
            "b" => $b,
            "c" => $c,
            "angle_A" => $angle_A,
            "angle_B" => $angle_B,
            "angle_C" => $angle_C,
        ];

        return $answer;
    }

    /**
     * Find side and angles of triangle using two sides and angle opposite one of them
     * Two answers are possible if a < b
     *
     * @param $a
     * @param $b
     * @param $angle_A
     * @return array|string
     */
    public function sideSideAngle($a, $b, $angle_A)
    {
        $angle_C = $this->sinAngle($a, $b, $angle_A);

        if ($angle_C>1) {
            $answer = 'Ошибка! Синус угла больше единицы';
        }elseif ($angle_C<=1) {
            $angle_C = asin($angle_C);
            $angle_C = ceil((180*$angle_C)/pi());
            if ($a<$b) {

                $angle_C_one = $angle_C;
                $angle_B_one = $this->thirdAngle($angle_A, $angle_C_one);
                $c_one = $this->sinSide($a, $angle_C_one, $angle_A);
                $answer_one = [
                    "a" => $a,
                    "b" => $b,
                    "c" => $c_one,
                    "angle_A" => $angle_A,
                    "angle_B" => $angle_B_one,
                    "angle_C" => $angle_C_one,
                    ];

                $angle_C_two = 180 - $angle_C_one;
                $angle_B_two = $this->thirdAngle($angle_A, $angle_C_two);
                $c_two = $this->sinSide($a, $angle_C_two, $angle_A);
                $answer_two = [
                    "a" => $a,
                    "b" => $b,
                    "c" => $c_two,
                    "angle_A" => $angle_A,
                    "angle_B" => $angle_B_two,
                    "angle_C" => $angle_C_two,
                ];

                $answer = [$answer_one, $answer_two];
            }elseif ($a>=$b) {
                $angle_B = $this->thirdAngle($angle_A, $angle_C);
                $c = $this->sinSide($a, $angle_C, $angle_A);

                $answer = [
                    "a" => $a,
                    "b" => $b,
                    "c" => $c,
                    "angle_A" => $angle_A,
                    "angle_B" => $angle_B,
                    "angle_C" => $angle_C,
                ];
            }
        }

        return $answer;
    }

    /**
     * Find sides and angle of triangle using two angles and side between them
     *
     * @param $c
     * @param $angle_A
     * @param $angle_B
     * @return array|string
     */
    public function angleSideAngle($c, $angle_A, $angle_B)
    {
        if (($angle_A+$angle_B)>180) {
            $answer = 'Ошибка! Построение треугольника невозможно, проверьте данные';
        }else {
            $angle_C = $this->thirdAngle($angle_A, $angle_B);
            $a = $this->sinSide($c, $angle_A, $angle_C);
            $b = $this->sinSide($c, $angle_B, $angle_C);

            $answer = [
                "a" => $a,
                "b" => $b,
                "c" => $c,
                "angle_A" => $angle_A,
                "angle_B" => $angle_B,
                "angle_C" => $angle_C,
            ];
        }

        return $answer;
    }
}