<?php


use Davihedg\Triangle\Theorems;
use PHPUnit\Framework\TestCase;

class TheoremsTest extends TestCase
{
    public function testTrueIsTrue()
    {
        $foo = true;
        $this->assertTrue($foo);
    }

    public function testCosAngle()
    {
        $a=12;
        $b=8;
        $c=11;

        $answer = new Theorems;
        $angle = $answer->cosAngle($a, $b, $c);

        $this->assertEquals($angle, 77);
    }

    public function testCosSide()
    {
        $a=6;
        $b=8;
        $angle_C=45;

        $answer = new Theorems;
        $side = $answer->cosSide($a, $b, $angle_C);

        $this->assertEquals($side, 11);
    }

    public function testSinAngle()
    {
        $a=6;
        $b=8;
        $angle_A=30;

        $answer = new Theorems;
        $side = $answer->sinAngle($a, $b, $angle_A);

        $this->assertEquals($side, 0.7);
    }

    public function testSinSide()
    {
        $b=5;
        $angle_A=45;
        $angle_B=105;

        $answer = new Theorems;
        $side = $answer->sinSide($b, $angle_A, $angle_B);

        $this->assertEquals($side, 3.7);
    }
}
