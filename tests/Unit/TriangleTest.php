<?php

use Davihedg\Triangle\Triangle;

class TriangleTest extends \PHPUnit\Framework\TestCase
{
    public function testTrueIsTrue()
    {
        $foo = true;
        $this->assertTrue($foo);
    }

    public function testSuccess()
    {
        $arguments = [
            "a" => NULL,
            "b" => NULL,
            "c" => 5,
            "angle_A" => 30,
            "angle_B" => 45,
            "angle_C" => NULL,
        ];


        $answer = new Triangle;
        $triangle = $answer->character($arguments);

        $this->assertEquals($triangle, [2.6, 3.7, 5, 30, 45, 105]);
    }

    public function testFindingPerimeter()
    {
        $arguments = [
            "a" => 1,
            "b" => 2,
            "c" => 3,
            "angle_A" => NULL,
            "angle_B" => NULL,
            "angle_C" => NULL,
        ];

        $answer = new Triangle;
        $perimeter = $answer->perimeter($arguments);

        $this->assertEquals($perimeter, 6);
    }

    public function testFindingSquare()
    {
        $arguments = [
            "a" => 9,
            "b" => 8,
            "c" => NULL,
            "angle_A" => NULL,
            "angle_B" => NULL,
            "angle_C" => 30,
        ];

        $answer = new Triangle;
        $square = $answer->square($arguments);

        $this->assertEquals($square, 18);
    }
}
