<?php


use Davihedg\Triangle\Character;
use PHPUnit\Framework\TestCase;

class CharacterTest extends TestCase
{
    public function testTrueIsTrue()
    {
        $foo = true;
        $this->assertTrue($foo);
    }

    public function testFindingAngles()
    {
        $a = 2;
        $b = 3;
        $c = 4;

        $answer = new Character();
        $triangle = $answer->threeSide($a, $b, $c);

        $equal = [
        "a" => 2,
        "b" => 3,
        "c" => 4,
        "angle_A" => 29,
        "angle_B" => 47,
        "angle_C" => 104,
    ];

        $this->assertEquals($triangle, $equal);
    }

    public function testFindingSideAndAngles()
    {
        $a = 12;
        $b = 8;
        $angle_C = 60;

        $answer = new Character;
        $triangle = $answer->sideAngleSide($a, $b, $angle_C);

        $equal = [
            "a" => 12,
            "b" => 8,
            "c" => 11,
            "angle_A" => 77,
            "angle_B" => 43,
            "angle_C" => 60,
        ];

        $this->assertEquals($triangle, $equal);
    }

    public function testFindingAnglesAndSide()
    {
        $a=6;
        $b=8;
        $angle_A=30;

        $answer = new Character;
        $triangle = $answer->sideSideAngle($a, $b, $angle_A);

        $equal_one = [
            "a" => 6,
            "b" => 8,
            "c" => 8.5,
            "angle_A" => 30,
            "angle_B" => 105.0,
            "angle_C" => 45,
        ];
        $equal_two = [
            "a" => 6,
            "b" => 8,
            "c" => 8.5,
            "angle_A" => 30,
            "angle_B" => 15.0,
            "angle_C" => 135.0,
        ];
        $equal = [$equal_one, $equal_two];

        $this->assertEquals($triangle, $equal);
    }

    public function testFindingAngleAndSides()
    {
        $c=5;
        $angle_A=30;
        $angle_B=45;

        $answer = new Character;
        $triangle = $answer->angleSideAngle($c, $angle_A, $angle_B);

        $equal = [
            "a" => 2.6,
            "b" => 3.7,
            "c" => 5,
            "angle_A" => 30,
            "angle_B" => 45,
            "angle_C" => 105,
        ];

        $this->assertEquals($triangle, $equal);
    }

    public function testErrors()
    {
        $a=6;
        $b=8;
        $angle_A=60;
        $c=25;
        $angle_B=135;

        $answer = new Character;
        $error_one = $answer->sideSideAngle($a, $b, $angle_A);
        $error_two = $answer->angleSideAngle($c, $angle_A, $angle_B);
        $error_three = $answer->threeSide($a, $b, $c);

        $this->assertEquals($error_three, 'Ошибка! Построение треугольника невозможно, проверьте данные');
        $this->assertEquals($error_two, 'Ошибка! Построение треугольника невозможно, проверьте данные');
        $this->assertEquals($error_one, 'Ошибка! Синус угла больше единицы');
    }
}
